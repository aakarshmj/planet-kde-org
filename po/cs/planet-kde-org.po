#
# Vit Pelcak <vit@pelcak.org>, 2021, 2023.
msgid ""
msgstr ""
"Project-Id-Version: planet-kde-org 1.0\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-05 12:42+0000\n"
"PO-Revision-Date: 2023-01-03 16:14+0100\n"
"Last-Translator: Vit Pelcak <vit@pelcak.org>\n"
"Language-Team: Czech <kde-i18n-doc@kde.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Lokalize 22.12.0\n"

#: config.yaml:0
msgid "Planet KDE"
msgstr "Planeta KDE"

#: config.yaml:0
msgid "Planet KDE site providing newest news from the KDE Project"
msgstr ""

#: config.yaml:0
msgid "Add your own feed"
msgstr "Přidat do vlastního kanálu"

#: i18n/en.yaml:0
msgid "Welcome to Planet KDE"
msgstr "Vítejte v planetě KDE"

#: i18n/en.yaml:0
msgid ""
"This is a feed aggregator that collects what the contributors to the [KDE "
"community](https://kde.org) are writing on their respective blogs, in "
"different languages"
msgstr ""

#: i18n/en.yaml:0
msgid "Go down one post"
msgstr ""

#: i18n/en.yaml:0
msgid "Go up one post"
msgstr ""
