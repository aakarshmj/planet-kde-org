<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="3.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:atom="http://www.w3.org/2005/Atom">
  <xsl:output method="html" version="1.0" encoding="UTF-8" indent="yes"/>
  <xsl:template match="/">
    <html xmlns="http://www.w3.org/1999/xhtml" lang="en">
      <head>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
        <title><xsl:value-of select="/rss/channel/title"/> RSS Feed Preview</title>
      </head>
      <body>
        <p>
          This is an <strong>RSS feed</strong>. <strong>Subscribe</strong> by copying the URL into your feedreader.
        </p>
        <div>
          <header>
            <h1>
              <img src="{/rss/channel/image/url}" width="{/rss/channel/image/width}" style="margin-right: 1.5rem;"/><xsl:value-of select="/rss/channel/title"/> RSS Feed Preview
            </h1>
            <p><xsl:value-of select="/rss/channel/description"/></p>
            <a target="_blank" href="{/rss/channel/link}">Visit website &#x238b;</a>
          </header>
          <h2>Items</h2>
          <xsl:for-each select="/rss/channel/item">
            <div>
              <h3>
                <a target="_blank" href="{link}">
                  <xsl:value-of select="title"/> &#x238b;
                </a>
              </h3>
              <small>
                Author: <xsl:value-of select="author" /><br/>
                Published: <xsl:value-of select="pubDate" />
              </small>
            </div>
          </xsl:for-each>
        </div>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
